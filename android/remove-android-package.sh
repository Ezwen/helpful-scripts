#!/bin/zsh

set -e

function delete_folders_from_field {
	PATHS=($(adb shell dumpsys package packages | grep -A18 "Package \[$1\]" | grep $2 | sed "s/$2=//"))
	for P in $PATHS; do
		PATHCLEAN=${P[1,-2]}
		echo "Removing ${PATHCLEAN}"
		adb shell "su -c \"rm -r ${PATHCLEAN}\""
	done
}

function nuke {
	delete_folders_from_field "$1" "codePath"
	delete_folders_from_field "$1" "dataDir"
}

function remove_package {
	adb shell "su -c \"mount -o rw,remount /system\""
	echo "===== App $1 ====="
	adb shell "su -c \"am force-stop $1\""
	nuke "$1"
}

if [[ $1 == "" ]]; then
	echo "Please provide a package name."
	exit 0
fi

remove_package "$1"
