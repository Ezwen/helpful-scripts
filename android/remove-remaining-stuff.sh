#!/bin/zsh

set -e

zsh remove-android-package.sh "com.emoji.keyboard.touchpal"
zsh remove-android-package.sh "com.cleanmaster.mguard"
zsh remove-android-package.sh "com.ape.filemanager"
zsh remove-android-package.sh "com.ape.weather"
