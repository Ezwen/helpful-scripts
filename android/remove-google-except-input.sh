#!/bin/zsh

set -e

# Remove everything with "google" in the id, except input
TOMOVE=($(adb shell pm list packages | grep "google" | sed 's/package://' | sed 's/[[:space:]]$//' | grep -v "com.google.android.inputmethod.latin"))
for p in $TOMOVE; do
	zsh remove-android-package.sh $p
done

# Remove the play store, which soes not have "google" in the id
zsh remove-android-package.sh "com.android.vending"
