#!/bin/env python

import sys
import os
from subprocess import call

#f = open('/tmp/python-dia2pdf-debug.log', 'w')
#sys.stdout=f
#sys.stderr=f



# Getting files selected in nautilus
files = os.environ.get('NAUTILUS_SCRIPT_SELECTED_FILE_PATHS')

# Getting the script path, to find dia2pdf in a relative way
scriptFolder = os.path.dirname(os.path.realpath(sys.argv[0]))
diaFileMaybe = scriptFolder + "/../bash/dia2pdf"

# Filtering files to only keep dia files
valid_files = [elem for elem in files.split('\n') 
    if elem != "" 
    and os.path.isfile(elem) 
    and elem.endswith(".dia")  ]

# Then for each file
for file in valid_files:
    print(file)

    # Find dir to work in
    dir = os.path.dirname(file)
    os.chdir(dir)

    # Call dia2pdf, should be in path (e.g. symbolic link in /usr/local/bin)
    basefile = os.path.basename(file)
    if os.path.isfile(diaFileMaybe):
        call([diaFileMaybe, basefile])#, stdout=f, stderr=f)
    else:
        call(["dia2pdf", basefile])#, stdout=f, stderr=f)

#f.close()
