#!/bin/env python

import os
from subprocess import call

# Get files selected in nautilus
files = os.environ.get('NAUTILUS_SCRIPT_SELECTED_FILE_PATHS')

# Remove empty strings
valid_files = [elem for elem in files.split('\n') if elem != "" ]

# Get the target folder
target = valid_files[0]
if (os.path.isfile(target)):
    target = os.path.dirname(target)

# Open a new tab in guake in the taregt folder
call(["guake", "-n", target])
