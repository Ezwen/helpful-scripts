To use the script, make them available in `~/.local/share/nautilus/scripts/`.

Either like this:

```
rmdir ~/.local/share/nautilus/scripts/
ln -s /path/to/helpful-scripts/nautilusScripts ~/.local/share/nautilus/scripts/
```

Or like this (which allow the remaning of the links, for better UI menus):

```
cd ~/.local/share/nautilus/scripts/
ln -s /path/to/helpful-scripts/nautilusScripts/*
```

See here for documentation: https://help.ubuntu.com/community/NautilusScriptsHowto
