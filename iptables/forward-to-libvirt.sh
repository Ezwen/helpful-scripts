#/bin/env bash


# Put VM network interface in "internal" zone
 firewall-cmd --zone=internal --change-interface=virbr0

# Make physical interface do port forwarding
firewall-cmd --zone=FedoraWorkstation --add-masquerade
firewall-cmd --zone=FedoraWorkstation --add-forward-port=port=80:proto=tcp:toport=80:toaddr=192.168.122.113
firewall-cmd --zone=FedoraWorkstation --add-forward-port=port=443:proto=tcp:toport=443:toaddr=192.168.122.113

# Because of libvirt, additionnal uptables rules are required
# TODO find the equivalent with firewalld?
iptables -t filter -I FORWARD -d 192.168.122.113/32 -p tcp -m state --state NEW -m tcp --dport 443 -j ACCEPT
iptables -t nat -A PREROUTING -i 192.168.0.21 -p tcp --dport 443 -j DNAT --to 192.168.122.113:443
iptables -t filter -I FORWARD -d 192.168.122.113/32 -p tcp -m state --state NEW -m tcp --dport 80 -j ACCEPT
iptables -t nat -A PREROUTING -i 192.168.0.21 -p tcp --dport 80 -j DNAT --to 192.168.122.113:80
