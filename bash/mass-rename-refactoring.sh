#!/bin/env bash


echo "Be careful, this script is quite brutal. Always make a copy before usage."

# Checking amount of args and print help
if [ $# -lt 3 ]; then
	echo "Usage: $0 <sed_regexp_to_replace> <replacement_string> <folder_to_refactor>"
	exit 1
fi


# Getting args and getting absolute path
FROM=$1
TO=$2
FOLDER=$(realpath "$3")

# Testing folder arg
if [ -d "$FOLDER" ]; then
    echo "Refactoring $FOLDER..."
else
    echo "$FOLDER is not a folder."
    exit 1
fi

# Counting files
NBFILES=$(find . -type f | wc -l)

# Rename in the files
if [ "$NBFILES" -gt 0 ]; then
    echo "Replacing $FROM by $TO in each file content..."
    find "$FOLDER" -name ".git" -prune -o -type f -print0 | xargs -0 sed -i "s/$FROM/$TO/g"
else
    echo "No files to change."
fi

# Rename the files/folders themselves
echo "Replacing $FROM by $TO in each file/folder name..."
find "$FOLDER" -name ".git" -prune -o -depth -print0 | xargs -0 rename "$FROM" "$TO"

echo "Done."
