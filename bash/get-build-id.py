#!/bin/env python3

import sys, json

try: 
    print(json.load(sys.stdin)['id'])
except json.decoder.JSONDecodeError: 
    sys.stderr.write("Cannot parse JSON file, maybe the website is down?\n")
    sys.exit(1)
