#!/bin/bash

# Transformation into crappy GIF
ffmpeg -i "$1" -pix_fmt rgb8 -vf "scale=w=640:h=640:force_original_aspect_ratio=decrease,fps=fps=12" -r 24 -an tmp.gif

# Optimisation of the GIF
gifsicle -O3 tmp.gif -o "$2"

rm tmp.gif
