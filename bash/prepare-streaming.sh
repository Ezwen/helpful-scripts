#!/bin/bash

set -x
set -e

##################################### RTPM

function start_rtmp() {
	# Start streaming server
	podman run --name rtmp-hls -d --rm -p 1935:1935 -p 8080:8080 alqutami/rtmp-hls:latest-alpine
}

function stop_rtmp() {
	podman rm -f rtmp-hls
}

function isrunning_rtmp() {
	podman ps | grep rtmp > /dev/null
}

##################################### RTPM end
##################################### fakesound

function start_fakesound() {
	# Start fake sound card
	pactl load-module module-null-sink sink_name="webstream_sink" sink_properties=device.description="web_stream"

	# Get real sound card name
	REAL_SOUNDCARD=$(pactl list short sinks | sed -n 's/^0\s\(\S*\).*$/\1/p')

	# Redirect fake sound card → real sound card
	pactl load-module module-loopback source=webstream_sink.monitor sink="$REAL_SOUNDCARD" rate=44100
}

function stop_fakesound() {
	pactl unload-module module-null-sink
}

function isrunning_fakesound() {
	pactl list short sinks | grep webstream > /dev/null
}
##################################### fakesound end





function startme() {

	if ! isrunning_rtmp ; then
		start_rtmp
	fi
	
	if ! isrunning_fakesound ; then
		start_fakesound
	fi

	# Start pavucontrol to redirect the app sound
	pavucontrol
}

function stopme() {

	if isrunning_rtmp ; then
		stop_rtmp
	fi
	
	if isrunning_fakesound ; then
		stop_fakesound
	fi

}

case "$1" in 
    start)   startme ;;
    stop)    stopme ;;
    restart) stopme; startme ;;
    *) echo "usage: $0 start|stop|restart" >&2
       exit 1
       ;;
esac
