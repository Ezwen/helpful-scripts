#!/bin/env bash

# Stop on error
set -e

# Getting installed build ID
if test -e gemoc-nightly-date; then
	OLDNIGHTLYDATE=$(cat gemoc-nightly-date)
else
	OLDNIGHTLYDATE="0001-01-01 01:00"
fi

# Getting lastest build ID
NEWNIGHTLYDATE=$(curl -s https://download.eclipse.org/gemoc/packages/nightly/ | grep -o 'Build date: .* UTC' | sed -e 's/Build date: //' | sed -e 's/,//')

echo "Installed build is from $OLDNIGHTLYDATE"
echo "Lastest build is from $NEWNIGHTLYDATE"

OLDNIGHTLYDATE_INT=$(date -d "$OLDNIGHTLYDATE" +%s)
NEWNIGHTLYDATE_INT=$(date -d "$NEWNIGHTLYDATE" +%s)

if test "$OLDNIGHTLYDATE_INT" = "$NEWNIGHTLYDATE_INT"; then
	echo "No need to update. Exiting." 
	exit 1
		
else
	echo "Updating to build from $NEWNIGHTLYDATE..."
fi

# Download new GEMOC Studio
echo "Downloading latest studio..."
wget -t 100 -c https://download.eclipse.org/gemoc/packages/nightly/gemoc_studio-linux.gtk.x86_64.zip

# Remove older eclipse (gemoc_studio-linux.gtk.x86_64.old)
echo "Deleting older studio backup (gemoc_studio-linux.gtk.x86_64.old)..."
rm -rf gemoc_studio-linux.gtk.x86_64.old

# Rename current eclipse to old
echo "Renaming old studio to gemoc_studio-linux.gtk.x86_64.old..."
mv gemoc_studio-linux.gtk.x86_64 gemoc_studio-linux.gtk.x86_64.old || true

# Extract
echo "Extracting new studio in gemoc_studio-linux.gtk.x86_64..."
unzip -d gemoc_studio-linux.gtk.x86_64 gemoc_studio-linux.gtk.x86_64.zip

# Remove downloaded zip
echo "Removing new studio zip (gemoc_studio-linux.gtk.x86_64.zip)..."
rm gemoc_studio-linux.gtk.x86_64.zip

# Storing new build ID
echo "Storing new build date in gemoc-build-id..."
echo "$NEWNIGHTLYDATE" > gemoc-nightly-date

echo "Done!"
