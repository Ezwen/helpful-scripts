#!/bin/env bash

echo "Shutting down unused network interface..."
sudo ip link set enp0s25 down && sudo ip link set virbr0 down && sudo ip link set wlp3s0 down

echo "Running powertop autotune..."
sudo powertop --auto-tune

echo "Setting powersave tuned profile..."
sudo tuned-adm profile powersave

echo "done"
