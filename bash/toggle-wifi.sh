#!/bin/bash

if [ "$(LANG=en nmcli r wifi)" = "disabled" ]; then
    nmcli r wifi on
else
    nmcli r wifi off
fi
