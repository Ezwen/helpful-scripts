//usr/bin/env jshell "$0" "$@"; exit $?

/* dia2pdf
 * Usage: jshell -R-DdiaFile=<dia_file> dia2pdf.jsh
 * Converts a dia file to a pdf file.
 * Requires dia and inkscape in the PATH.
 */ 

// Load commons
/open commons.jsh

// Prepare tmp svg file path
Path svgTmpFile = Files.createTempFile("dia2pdf",".svg");
Files.delete(svgTmpFile);

// Start block (to be able to interrupt the block on an exception)
{ 

    // Getting and checking input file
    String diaFilePath = System.getProperty("diaFile");
    if (diaFilePath == null) {
        throw new Exception("Usage: jshell -R-DdiaFile=<dia_file> dia2pdf.jsh");
    }
    Path diaFile = Paths.get(diaFilePath);
    if (!Files.exists(diaFile)) { 
        throw new Exception("The file "+diaFile+" does not exist.");
    } 
    System.out.println("Processing "+diaFile+"...");
    
    // Calling dia to save tmp svg file
    runCommand("dia", "--export="+svgTmpFile, diaFile.toString());
    if (!Files.exists(svgTmpFile)) {
        throw new Exception("Error: the svg file was not produced by dia.");
    }

    // Calling inkscape to save final pdf
    String finalPdfPath = diaFilePath.substring(0,diaFilePath.length()-"dia".length()) + "pdf";
    Path finalPdf = Paths.get(finalPdfPath);
    runCommand("inkscape", "--without-gui", "--export-pdf="+finalPdf, "--export-area-drawing",  svgTmpFile.toString() );
    
    // Checking output
    if (!Files.exists(svgTmpFile)) {
        throw new Exception("Error: the pdf file was not produced by inkscape.");
    }

}

// Whatever happens, deletes tmp svg file
if (Files.exists(svgTmpFile)) {
    Files.delete(svgTmpFile);
}


/exit
