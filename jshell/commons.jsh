/**
 * Runs a system command and wait for it to finish.
 * @throws A generic exception if the command fails (ie. does not return 0).
 */
void runCommand(String... tokens) throws Exception{
    List<String> list = Arrays.asList(tokens);
    System.out.println("Running "+list+" ...");
    Process p = new ProcessBuilder(list).start();
    p.waitFor();
    if (p.exitValue() != 0) {
        throw new Exception("There was a problem when running "+list+".");
    }
}

